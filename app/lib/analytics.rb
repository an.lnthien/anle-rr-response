module Analytics
  # This is a thread-safe queue https://ruby-doc.org/core-2.7.0/Queue.html
  @@queue = Queue.new

  def self.event(event, value, event_time = Time.now)
    @@queue.push(
      event: event, value: value, event_time: event_time
    )
  end

  def self.consume_events
    # The idea is we insert data to db in each single batch of 25 events
    # If the batch has less than 25 events, we wait unit a timeout
    # If exceeding the timeout, we insert eveything we have in the batch
    Thread.new do
      records = []
      sleep_count = 0

      while true
        begin
          if records.length > 0 && (records.length % 25 == 0 || sleep_count == 3)
            Analytics.events(records)
            records = []
          end

          record = @@queue.pop(true)
          records << record

          sleep_count = 0
        rescue ThreadError
          sleep_count += 1
          sleep 1
        end
      end
    end

    (1..50).each do |index|
      event(:random, index)
    end
  end

  def self.events(events)
    columns = [:event, :value, :event_time]
    Analytic.import(columns, events)
  end
end
