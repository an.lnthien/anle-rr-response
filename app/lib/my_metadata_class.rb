module MetadataAccessor
  extend ActiveSupport::Concern

  class_methods do
    def metadata_accessor(attr, as: nil)
      define_method "#{attr}_set" do |keys, value|
        if self.send(attr).nil?
          self.send("#{attr}=", {})
        end

        split_key = keys.split('.')
        if split_key.length != 2
          raise "Required format: '<attr>set(key1.key2, value)'"
        end

        first_key = split_key[0]
        second_key = split_key[1]

        unless self.send(attr).key?(first_key)
          self.send(attr)[first_key] = {}
        end

        self.send(attr)[first_key][second_key] = value
      end

      define_method "#{attr}_get" do |keys|
        split_key = keys.split('.')
        if split_key.length != 2
          raise "Required format: '<attr>get(key1.key2)'"
        end

        first_key = split_key[0]
        second_key = split_key[1]

        return if self.send(attr).nil?

        first_hash = self.send(attr)[first_key]
        unless first_hash.nil?
          first_hash[second_key]
        end
      end

      unless as.nil?
        alias_method "#{as}_set", "#{attr}_set"
        alias_method "#{as}_get", "#{attr}_get"
      end
    end
  end
end

class MyMetadataClass
  include MetadataAccessor

  attr_accessor :temp_data
  metadata_accessor :temp_data, as: :metadata
end
