class EventWorker
  include Sidekiq::Worker

  def perform(event, value, event_time)
    Analytic.create!(event: event, value: value, event_time: event_time)
  end
end
