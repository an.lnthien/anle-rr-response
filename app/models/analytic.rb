class Analytic < ApplicationRecord
  # Note: this solution is applied to model level only. If someone tries to use raw query, they can bypass this restriction
  # e.g ActiveRecord::Base.connection.execute("update analytics set value='xyz' where id=1")
  # To be more strictly, we can configure access control in Database System
  def readonly?
    new_record? ? false : true
  end
end
