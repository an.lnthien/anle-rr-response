require 'rails_helper'
RSpec.describe EventWorker, type: :worker do
  describe 'perform' do
    before do
      EventWorker.new.perform("login", "anle", Time.zone.parse("Fri, 08 Apr 2022 00:56:40 UTC +00:00"))
    end

    it 'saves data to db' do
      record = Analytic.find_by(event: 'login', value: 'anle')
      expect(record.event_time.to_s).to eq('2022-04-08 00:56:40 UTC')
    end
  end
end
