require 'rails_helper'

RSpec.describe Analytics, type: :lib do
  describe '.event' do
    before do
      user = OpenStruct.new(nickname: "Tom")
      Analytics.event(:login, user.nickname)
    end

    it 'pushed event to a queue' do
      event = Analytics.class_variable_get(:@@queue).pop
      expect(event[:value]).to eq('Tom')
    end
  end

  describe '.events' do
    before do
      Analytics.events([
                         [:login, "Tom", Time.zone.parse("Fri, 08 Apr 2022 00:56:40 UTC +00:00")],
                         [:logout, "Peter", Time.zone.parse("Fri, 09 Apr 2022 00:56:40 UTC +00:00")]
                       ])

    end

    it 'imports many records in one insert' do
      record = Analytic.find_by(event: 'login', value: 'Tom')
      expect(record.event_time.to_s).to eq('2022-04-08 00:56:40 UTC')

      record = Analytic.find_by(event: 'logout', value: 'Peter')
      expect(record.event_time.to_s).to eq('2022-04-09 00:56:40 UTC')
    end
  end
end
