require 'rails_helper'

RSpec.describe MyMetadataClass, type: :lib do
  it do
    k = MyMetadataClass.new
    expect(k.temp_data).to eq(nil)

    k.metadata_set('user.nickname', 'sally')
    expect(k.temp_data).to eq("user"=>{"nickname"=>"sally"})
    expect(k.metadata_get('user.name')).to eq(nil)
    expect(k.metadata_get('user.nickname')).to eq('sally')

    k.metadata_set('schoold.name', 'MIT')
    expect(k.metadata_get('schoold.name')).to eq('MIT')

    expect(k.metadata_get('abc.def')).to eq(nil)
  end
end
