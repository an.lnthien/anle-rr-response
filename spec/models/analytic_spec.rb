require 'rails_helper'

RSpec.describe Analytic, type: :model do
  describe 'creating a record' do
    before do
      Analytic.create!(
        event: 'login',
        value: 'anle',
        event_time: Time.zone.parse("Fri, 08 Apr 2022 00:56:40 UTC +00:00"))
    end

    it 'saves data into db' do
      record = Analytic.find_by(event: 'login', value: 'anle')
      expect(record.event_time.to_s).to eq('2022-04-08 00:56:40 UTC')
    end

    it 'cannot update the record' do
      record = Analytic.find_by(event: 'login', value: 'anle')
      record.event_time = Time.now
      expect {
        record.save
      }.to raise_error(ActiveRecord::ReadOnlyRecord)
    end
  end
end
